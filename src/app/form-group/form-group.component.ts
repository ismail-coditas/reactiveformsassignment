import { Component, OnInit } from '@angular/core';
import {FormGroup , FormControl} from '@angular/forms'

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.scss']
})
export class FormGroupComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  
  registrationForm = new FormGroup({
    userName: new FormControl('faizan'),
    password: new FormControl(''),
    confirmPassword: new FormControl(''),

    address: new FormGroup({
      city: new FormControl(''),
      State: new FormControl(''),
      postalCode: new FormControl('')
    })
  })
  loadApiData(){
    this.registrationForm.setValue({
      userName:'Bruce',
      password:'test',
      confirmPassword:'test',
      address:{
        city:'City',
        State:'State',
        postalCode:'12345'
      }
    })
  }
}




import { Component, OnInit } from '@angular/core';
// import {FormGroup , FormControl} from '@angular/forms'
import {FormBuilder, Validators} from '@angular/forms'


@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss']
})
export class FormBuilderComponent implements OnInit {

  constructor(private fb:FormBuilder) { }
  
  ngOnInit(): void {
  }

    registrationForm = this.fb.group({
      userName:['faizan',Validators.required],
      password:[''],
      confirmPassword:[''],

      address:this.fb.group({
         city: [''],
         State: [''],
         postalCode: ['']        
      })
    })


    //   registrationForm = new FormGroup({
    //   userName: new FormControl('faizan'),
    //   password: new FormControl(''),
    //   confirmPassword: new FormControl(''),
  
    //   address: new FormGroup({
    //     city: new FormControl(''),
    //     State: new FormControl(''),
    //     postalCode: new FormControl('')
    //   })
    // })
    loadApiData(){
      this.registrationForm.setValue({
        userName:'Bruce',
        password:'test',
        confirmPassword:'test',
        address:{
          city:'City',
          State:'State',
          postalCode:'12345'
        }
      })
    }
  }






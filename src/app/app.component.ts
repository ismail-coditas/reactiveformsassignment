import { Component } from '@angular/core';
import { FormControl} from '@angular/forms'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  
    userName= new FormControl('faizan')
    password= new FormControl('')
    email= new FormControl('')
 
    date= new FormControl('')
    hobbies= new FormControl('')
    gender= new FormControl('')
    comments= new FormControl('')
    
  }
  
  